﻿# Installation

The following project was built using C# (.NET Core). It builds into a `.exe` file that can be run on Windows by default or on Linux using `wine` or `mono`.

## Requirements
1. Visual Studio 2019 or later
2. C# and .NET core libraries

## Build
1. Clone the repo at: TODO
2. In the project's root directory, open the `MarsRover.sln` file using Visual Studio 2019
3. Build the project by selecting **Build** > **Build Solution**

# Run

The project should output a `MarsRover.exe` file in the following directory:

`{rootDirectory}\MarsRover\MarsRover\bin\Debug\netcoreapp3.1\`

A command line will spawn accepting input in the same format shown on the challenge document, as shown below.

**Test Input:**

5 5

1 2 N

LMLMLMLMM

3 3 E

MMRMMRMRRM

**Expected Output:**

1 3 N

5 1 E

## Windows
This should be able to run on a modern Microsoft Windows workstation with no issues. 


## Linux
On Linux, the application can be run using `wine` or `mono`. If you are unfamiliar with these programs, you can raise an issue on the GitLab project or email me directly to run you through how to use these libraries.

# Assumptions

## Core Assumptions
1. Rovers can not fall of the plateau
2. Rovers that reach the end of the grid skip actions that would cause them to fall off
3. There are no other objects on the plateau (also see optional assumptions below)
4. Rovers are not inherently aware of other rovers

## Optional Assumptions
1. No more than one rover can occupy a grid block
2. Rovers take no damage
3. Rovers that collide immediately stop
4. Rovers have no momentum