﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MarsRover
{
    class Program
    {
        static void Main(string[] args)
        {
            // Core variables that will store all major objects
            Plateau plateau;
            List<Rover> rovers = new List<Rover>();
            List<string> commands = new List<string>();

            // Take plateau size first
            string input = Console.ReadLine();
            plateau = new Plateau(Int16.Parse(input.Split(" ").ElementAt(0)), Int16.Parse(input.Split(" ").ElementAt(1)));

            // Keep adding rover + command sets until empty input
            input = Console.ReadLine();
            int i = 0;
            while (!input.Equals(""))
            {
                if (i == 0)
                {
                    rovers.Add(new Rover(Int16.Parse(input.Split(" ").ElementAt(0)), Int16.Parse(input.Split(" ").ElementAt(1)), input.Split(" ").ElementAt(2)));
                }
                else
                {
                    commands.Add(input);
                }
                
                i++;
                if (i > 1) i = 0;
                input = Console.ReadLine();
            }

            // Loop over rovers +command sets
            for (int index = 0; index < rovers.Count; index++)
            {
                Rover currentRover = rovers.ElementAt(index);
                currentRover.plateau = plateau;
                string currentCommands = commands.ElementAt(index);

                foreach (char c in currentCommands.ToCharArray())
                {
                    currentRover.Move(c.ToString());
                }

                Console.WriteLine(currentRover.Position[0] + " " + currentRover.Position[1] + " " + currentRover.CurrentDirection);
            }

            // Base test case
            //Plateau plateau = new Plateau(5, 5);
            //Rover rover = new Rover(1, 2, "N");
            //rover.plateau = plateau;
            //Rover rover1 = new Rover(3, 3, "E");
            //rover1.plateau = plateau;


            // Rover 1 test cases            
            //rover.Move("L");
            //rover.Move("M");
            //rover.Move("L");
            //rover.Move("M");
            //rover.Move("L");
            //rover.Move("M");
            //rover.Move("L");
            //rover.Move("M");
            //rover.Move("M");
            //Console.WriteLine(rover.Position[0] + " " + rover.Position[1] + " " + rover.CurrentDirection);

            // Rover 2 test case
            //rover1.Move("M");
            //rover1.Move("M");
            //rover1.Move("R");
            //rover1.Move("M");
            //rover1.Move("M");
            //rover1.Move("R");
            //rover1.Move("M");
            //rover1.Move("R");
            //rover1.Move("R");
            //rover1.Move("M");
            //Console.WriteLine(rover1.Position[0] + " " + rover1.Position[1] + " " + rover1.CurrentDirection);
        }
    }
}
