﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover
{
    class Rover
    {
        public Plateau plateau;

        private enum Direction
        {
            N, E, S, W
        }

        Direction currentDirection;

        public string CurrentDirection { 
            get { return currentDirection.ToString(); }
        }

        Dictionary<Direction, int> directionMappings = new Dictionary<Direction, int>() { { Direction.N, 0 }, { Direction.E, 90 }, { Direction.S, 180 }, { Direction.W, 270 } };

        int[] position;
        public int[] Position
        {
            get { return position; }
        }

        public Rover(int startingPosX, int startingPosY, string direction)
        {
            position = new int[2];
            position[0] = startingPosX;
            position[1] = startingPosY;

            switch (direction)
            {
                case "N":
                    currentDirection = Direction.N;
                    break;
                case "E":
                    currentDirection = Direction.E;
                    break;
                case "S":
                    currentDirection = Direction.S;
                    break;
                case "W":
                    currentDirection = Direction.W;
                    break;

            }
        }

        public void Move(string command)
        {
            switch (command)
            {
                case "L":
                    Rotate("L");
                    break;
                case "R":
                    Rotate("R");
                    break;
                case "M":
                    if (AtEdge()) // can't move if at edge given current direction
                    {
                        break;
                    }
                    if (currentDirection == Direction.N)
                    {
                        position[1] += 1;
                    }
                    else if (currentDirection == Direction.E)
                    {
                        position[0] += 1;
                    }
                    else if (currentDirection == Direction.S)
                    {
                        position[1] -= 1;
                    }
                    else
                    {
                        position[0] -= 1;
                    }

                    break;
            }
        }

        // TODO
        private bool AtEdge()
        {
            switch (currentDirection)
            {
                case Direction.N:
                    return position[1] == plateau.Edges[1];
                case Direction.E:
                    return position[0] == plateau.Edges[0];
                case Direction.S:
                    return position[1] == 0;
                case Direction.W:
                    return position[0] == 0;
            }
            return false;
        }

        // TODO
        private void Rotate(string rotation)
        {
            int currentRotation;
            directionMappings.TryGetValue(currentDirection, out currentRotation);

            // First calculate direction using angle mappings
            if (rotation.Equals("L")){
                currentRotation -= 90;
                if (currentRotation == -90) currentRotation = 270;
            }
            else
            {
                currentRotation += 90;
                if (currentRotation == 360) currentRotation = 0;
            }

            // Then set current direction
            foreach (Direction d in directionMappings.Keys)
            {
                int tempRotation;
                directionMappings.TryGetValue(d, out tempRotation);
                if (tempRotation == currentRotation)
                {
                    currentDirection = d;
                }
            }

        }
            
    }
}
