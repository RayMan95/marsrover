﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover
{
    public class Plateau
    {

        int[,] grid;
        int[] edges;

        public int[,] Grid
        {
            get { return grid; }
        }

        public int[] Edges
        {
            get { return edges;  }
        }


        public Plateau(int xLen, int yLen)
        {
            grid = new int[xLen, yLen];
            edges = new int[] { xLen, yLen };
        }

    }
}
